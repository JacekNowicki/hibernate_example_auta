package model;

public enum CarBodyType {
    SEDAN,
    COMBI,
    CABRIO
}
