package model;

import jdk.nashorn.internal.objects.annotations.Constructor;
import lombok.*;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
public class Car implements IBaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String brand;
    private String model;
    private LocalDate productionDate;
    @Enumerated(EnumType.ORDINAL)
    private CarBodyType carBodyType;
    private String color;
    private Long km;

    @UpdateTimestamp
    private LocalDateTime  modifiedDate;

    public Car(String brand, String model, LocalDate productionDate, CarBodyType carBodyType, String color, Long km) {
        this.brand = brand;
        this.model = model;
        this.productionDate = productionDate;
        this.carBodyType = carBodyType;
        this.color = color;
        this.km = km;
    }
}
