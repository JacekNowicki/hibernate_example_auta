import db.CarDAO;
import model.Car;
import model.CarBodyType;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        CarDAO carDAO = new CarDAO();
        Car car = new Car();

        String command;


        do {
            System.out.println("Podaj komendę:");
            command = scanner.next();

            if (command.equalsIgnoreCase("add_car")) {
                System.out.println("Dodajemy samochod:");
                System.out.println("Podaj markę:");
                String name = scanner.next();
                System.out.println("Podaj model:");
                String model = scanner.next();
                System.out.println("Podaj rok produkcji:");
                System.out.println("Podaj rok:");
                String year = scanner.next();
                System.out.println("Podaj miesiąc:");
                String month = scanner.next();
                System.out.println("Podaj dzien:");
                String day = scanner.next();
                LocalDate productionDate = LocalDate.of(Integer.valueOf(year),Integer.valueOf(month),Integer.valueOf(day));
                CarBodyType carBodyType = null;
                boolean correctEntry = false;
                do {
                    System.out.println("Podaj styl karoserii:");
                    System.out.println("1 - Sedan, 2 - Combi, 3 - Cabrio");
                    String entry = scanner.next();

                    switch (entry) {
                        case "1":
                            carBodyType = (CarBodyType.SEDAN);
                            correctEntry = true;
                            break;
                        case "2":
                            carBodyType = (CarBodyType.COMBI);
                            correctEntry = true;
                            break;
                        case "3":
                            carBodyType = (CarBodyType.CABRIO);
                            correctEntry = true;
                            break;
                    }
                }
                while (correctEntry == false);

                System.out.println("Podaj kolor:");
                String color = scanner.next();

                System.out.println("Podaj przejechane kilometry:");
                Long km = scanner.nextLong();

                carDAO.save(new Car(name,model,productionDate,carBodyType, color,km));
            }

        } while(!command.equalsIgnoreCase("stop"));




}

}
