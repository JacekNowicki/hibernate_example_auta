package db;

import model.IBaseEntity;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class CarDAO {

//- tworzenie auta

public void save (IBaseEntity objectToSave) {
    SessionFactory sessionFactory = HibernateUtil.getSessionFactory();

    Transaction transaction = null;

    try (Session session = sessionFactory.openSession()) {
        transaction = session.beginTransaction();

        session.saveOrUpdate(objectToSave);

        transaction.commit();
    } catch (Exception sqle) {// dzięki try - with - resources nie musimy robić 'close' na sesji
        if (transaction != null) {
            transaction.rollback();
        }
    }

    sessionFactory.close();

}


//- odczytywanie auta po id
//- zmianę parametrów auta
//- usuwanie auta
//- odczytywanie wszystkich aut (zaproponuj kilka sortowań)
//- odczytywanie aut wg. marki, typu nadwozia, daty produkcji
//- wyszukanie najstarszego/najmłodszego samochodu
//- wyszukanie samochodu o największym/najmniejszym przebiegu

}
